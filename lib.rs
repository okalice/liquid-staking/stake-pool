#![cfg_attr(not(feature = "std"), no_std, no_main)]

use ink::codegen::Env;
use ink::prelude::{boxed::Box, vec, vec::Vec};
use ink::primitives::AccountId;
use scale::{Decode, Encode};
use sp_runtime::MultiAddress;
use xcm::{
    v3::{prelude::*, Weight},
    VersionedMultiAsset, VersionedMultiAssets, VersionedMultiLocation, VersionedXcm,
};

/// Foreign parachain types
mod types {
    use super::*;

    #[derive(scale::Encode)]
    pub enum RuntimeCall {
        #[codec(index = 29)]
        NominationPools(NominationPoolsCall),
        #[codec(index = 31)]
        Balances(BalancesCall),
        #[codec(index = 51)]
        PolkadotXcm(PolkadotXcmCall),
        #[codec(index = 55)]
        XTokens(XTokensCall),
    }

    #[derive(scale::Encode)]
    pub enum BalancesCall {
        /// This index can be found by investigating the pallet dispatchable API. In your
        /// pallet code, look for `#[pallet::call]` section and check
        /// `#[pallet::call_index(x)]` attribute of the call. If these attributes are
        /// missing, use source-code order (0-based).
        #[codec(index = 0)]
        Transfer {
            dest: MultiAddress<AccountId, ()>,
            #[codec(compact)]
            value: u128,
        },
    }

    #[derive(scale::Encode)]
    pub enum PolkadotXcmCall {
        #[codec(index = 0)]
        Send {
            dest: Box<VersionedMultiLocation>,
            message: Box<VersionedXcm<()>>,
        },
    }

    #[derive(scale::Encode)]
    pub enum NominationPoolsCall {
        // #[codec(index = 0u8)]
        // Join {
        //     #[codec(compact)]
        //     amount: BalanceOf<T>,
        //     pool_id: PoolId,
        // },
        //
        // #[codec(index = 1u8)]
        // BondExtra {
        //     extra: BondExtra<BalanceOf<T>>,
        // },
        //
        // #[codec(index = 2u8)]
        // ClaimPayout {},
        //
        // #[codec(index = 3u8)]
        // Unbond {
        //     member_account: AccountIdLookupOf<T>,
        //     #[codec(compact)]
        //     unbonding_points: BalanceOf<T>,
        // },
        //
        // #[codec(index = 4u8)]
        // PoolWithdrawUnbonded {
        //     pool_id: PoolId,
        //     num_slashing_spans: u32,
        // },
        //
        // #[codec(index = 5u8)]
        // WithdrawUnbonded {
        //     member_account: AccountIdLookupOf<T>,
        //     num_slashing_spans: u32,
        // },
        #[codec(index = 6u8)]
        Create {
            #[codec(compact)]
            amount: u128,
            root: AccountId,
            nominator: AccountId,
            bouncer: AccountId,
        },
        // #[codec(index = 7u8)]
        // CreateWithPoolId {
        //     #[codec(compact)]
        //     amount: BalanceOf<T>,
        //     root: AccountIdLookupOf<T>,
        //     nominator: AccountIdLookupOf<T>,
        //     bouncer: AccountIdLookupOf<T>,
        //     pool_id: PoolId,
        // },
        //
        // #[codec(index = 8u8)]
        // Nominate {
        //     pool_id: PoolId,
        //     validators: Vec<T::AccountId>,
        // },
        //
        // #[codec(index = 12u8)]
        // UpdateRoles {
        //     pool_id: PoolId,
        //     new_root: ConfigOp<T::AccountId>,
        //     new_nominator: ConfigOp<T::AccountId>,
        //     new_bouncer: ConfigOp<T::AccountId>,
        // },
        //
        // #[codec(index = 13u8)]
        // Chill {
        //     pool_id: PoolId,
        // },
        //
        // #[codec(index = 14u8)]
        // BondExtraOther {
        //     member: AccountIdLookupOf<T>,
        //     extra: BondExtra<BalanceOf<T>>,
        // },
        //
        // #[codec(index = 15u8)]
        // set_claim_permission {
        //     permission: ClaimPermission,
        // },
        //
        // #[codec(index = 16u8)]
        // claim_payout_other {
        //     other: T::AccountId,
        // },
        //
        // #[codec(index = 17u8)]
        // set_commission {
        //     pool_id: PoolId,
        //     new_commission: Option<(Perbill, T::AccountId)>,
        // },
        //
        // #[codec(index = 18u8)]
        // set_commission_max {
        //     pool_id: PoolId,
        //     max_commission: Perbill,
        // },
        //
        // #[codec(index = 19u8)]
        // set_commission_change_rate {
        //     pool_id: PoolId,
        //     change_rate: CommissionChangeRate<BlockNumberFor<T>>,
        // },
        //
        // #[codec(index = 20u8)]
        // claim_commission {
        //     pool_id: PoolId,
        // },
        //
        // #[codec(index = 21u8)]
        // adjust_pool_deposit {
        //     pool_id: PoolId,
        // },
        //
        // #[codec(index = 22u8)]
        // set_commission_claim_permission {
        //     pool_id: PoolId,
        //     permission: Option<CommissionClaimPermission<T::AccountId>>,
        // },
    }

    #[derive(scale::Encode)]
    pub enum XTokensCall {
        #[codec(index = 1u8)]
        TransferMultiAsset {
            asset: Box<VersionedMultiAsset>,
            dest: Box<VersionedMultiLocation>,
            dest_weight_limit: WeightLimit,
        },
    }
}

#[ink::contract]
mod stake_pool {
    use super::*;

    #[ink(storage)]
    #[derive(Default)]
    pub struct StakePool;

    #[derive(Debug, PartialEq, Eq, scale::Encode, scale::Decode)]
    #[cfg_attr(feature = "std", derive(scale_info::TypeInfo))]
    pub enum RuntimeError {
        CallRuntimeFailed,
    }

    use ink::env::Error as EnvError;

    impl From<EnvError> for RuntimeError {
        fn from(e: EnvError) -> Self {
            match e {
                EnvError::CallRuntimeFailed => RuntimeError::CallRuntimeFailed,
                _ => panic!("Unexpected error from `pallet-contracts`."),
            }
        }
    }

    /// All the fees and weights values required for the whole
    /// operation.
    #[derive(Encode, Decode, Debug, Clone)]
    #[cfg_attr(feature = "std", derive(scale_info::TypeInfo))]
    pub struct WeightsAndFees {
        /// Max fee for whole XCM operation in foreign chain
        /// This includes fees for sending XCM back to original
        /// chain via Transact(pallet_xcm::send).
        pub foreign_base_fee: MultiAsset,
        /// Max weight for operation (remark)
        pub foreign_transact_weight: Weight,
        /// Max weight for Transact(pallet_xcm::send) operation
        pub foreign_transcat_pallet_xcm: Weight,
        /// Max fee for the callback operation
        /// send by foreign chain
        pub here_callback_base_fee: MultiAsset,
        /// Max weight for Transact(pallet_contracts::call)
        pub here_callback_transact_weight: Weight,
        /// Max weight for contract call
        pub here_callback_contract_weight: Weight,
    }

    #[ink(event)]
    pub struct XCMSent {}

    #[ink(event)]
    pub struct XCMFailed {}

    impl StakePool {
        /// Constructor that initializes the `bool` value to the given `init_value`.
        #[ink(constructor, payable)]
        pub fn new() -> Self {
            Default::default()
        }

        /// Tries to transfer `value` from the contract's balance to `receiver`.
        ///
        /// Fails if:
        ///  - called in the off-chain environment
        ///  - the chain forbids contracts to call `Balances::transfer` (`CallFilter` is
        ///    too restrictive)
        ///  - after the transfer, `receiver` doesn't have at least existential deposit
        ///  - the contract doesn't have enough balance
        #[ink(message)]
        pub fn basic_transfer(
            &mut self,
            receiver: AccountId,
            value: Balance,
        ) -> Result<(), RuntimeError> {
            self.env()
                .call_runtime(&types::RuntimeCall::Balances(
                    types::BalancesCall::Transfer {
                        dest: receiver.into(),
                        value,
                    },
                ))
                .map_err(Into::into)
        }

        /// Attempt to create pool on given parachain by
        /// sending a XCM using `call_runtime`.
        #[ink(message)]
        pub fn transfer_to_relay_contract_acc(&mut self, value: u128) -> Result<(), RuntimeError> {
            ink::env::debug_println!(
                "[1/3] Start of trying to transfer amount from astar to relay pool"
            );

            let asset: Box<VersionedMultiAsset> = Box::new(VersionedMultiAsset::V3(MultiAsset {
                id: Concrete(MultiLocation {
                    parents: 1,
                    interior: Here,
                }),
                fun: Fungible(value),
            }));

            let contract_account = self.env().account_id();
            let account_id: &[u8; 32] = contract_account.as_ref();
            let dest: Box<VersionedMultiLocation> = Box::new(
                (
                    Parent,
                    AccountId32 {
                        network: Some(Westend),
                        id: *account_id,
                    },
                )
                    .into(),
            );

            ink::env::debug_println!("[2/3] XCM Build successfully, sending...");

            // let xtransfer =
            self.env()
                .call_runtime(&types::RuntimeCall::XTokens(
                    types::XTokensCall::TransferMultiAsset {
                        asset,
                        dest,
                        dest_weight_limit: Unlimited,
                    },
                ))
                .map_err(Into::into)
        }

        #[ink(message)]
        pub fn create_pool(&mut self, value: u128) -> Result<(), crate::stake_pool::RuntimeError> {
            ink::env::debug_println!(
                "[1/3] create pool"
            );
            let dot = Concrete(MultiLocation {
                parents: 1,
                interior: Here,
            });
            let contract_account = self.env().account_id();
            let account_id: &[u8; 32] = contract_account.as_ref();

            let instructions = Xcm(vec![
                // buy execution with foreign asset
                DescendOrigin(X1(AccountId32 {
                    network: Some(Westend),
                    id: *account_id,
                })),
                WithdrawAsset(
                    MultiAsset {
                        id: dot,
                        fun: Fungible(value),
                    }
                    .into(),
                ),
                BuyExecution {
                    fees: MultiAsset {
                        id: dot,
                        fun: Fungible(2_000_000_000_000),
                    },
                    weight_limit: Unlimited,
                },
                // set on error handler
                // SetErrorHandler(self.error_handler()),
                // set on success handler
                // SetAppendix(self.success_handler()),
                // perform operation - np::create
                Transact {
                    origin_kind: OriginKind::SovereignAccount,
                    require_weight_at_most: Weight::from_parts(1_000_000, 100_000),
                    call: types::RuntimeCall::NominationPools(types::NominationPoolsCall::Create {
                        amount: value,
                        root: contract_account,
                        nominator: contract_account,
                        bouncer: contract_account,
                    })
                    .encode()
                    .into(),
                },
            ]);

            // FIXME: What will be the destination?
            let dest: Box<VersionedMultiLocation> = Box::new((Parent, Here).into());
            let message: Box<VersionedXcm<()>> = Box::new(VersionedXcm::V3(instructions));

            self.env()
                .call_runtime(&types::RuntimeCall::PolkadotXcm(
                    types::PolkadotXcmCall::Send { dest, message },
                ))
                .map_err(Into::into)
        }

        #[ink(message, selector = 0x00003333)]
        pub fn handle_response(&mut self, id: u32, success: bool) {
            ink::env::debug_println!("[1/1] Inside handle_response...");

            // self.result = Some(success);
        }

        fn error_handler(&self) -> Xcm<()> {
            // FIXME: Add error handler
            // let callback_xcm = Xcm(vec![
            //     // buy execution
            //     WithdrawAsset(Here.into()),
            //     BuyExecution {
            //         fees: MultiAsset {
            //             id: Here.into(),
            //             fun: Fungible(2_000_000_000_000),
            //         },
            //         weight_limit: Unlimited,
            //     },
            //     // transact call to contract method
            //     Transact {
            //         origin_kind: OriginKind::SovereignAccount,
            //         require_weight_at_most: Weight::from_parts(100000, 100000),
            //         call: here::RuntimeCall::Contracts(here::ContractsCall::Call {
            //             dest: self.env().account_id(),
            //             value: 0u128,
            //             gas_limit: weight_and_fees.here_callback_contract_weight,
            //             storage_deposit_limit: None,
            //             data: [[0x00, 0x00, 0x33, 0x33].to_vec(), success.encode()].concat(),
            //         })
            //             .encode()
            //             .into(),
            //     },
            //     ExpectTransactStatus(MaybeErrorCode::Success),
            // ]);
            //
            // Xcm(vec![Transact {
            //     origin_kind: OriginKind::SovereignAccount,
            //     require_weight_at_most: weight_and_fees.foreign_transcat_pallet_xcm,
            //     call: foreign::RuntimeCall::PolkadotXcm(foreign::PolkadotXcmCall::Send {
            //         dest: Box::new((Parent, Parachain(self.here_para_id)).into()),
            //         message: Box::new(VersionedXcm::V3(callback_xcm)),
            //     })
            //         .encode()
            //         .into(),
            // }])
            Xcm(vec![])
        }

        fn success_handler(&self) -> Xcm<()> {
            // FIXME: Add success handler
            Xcm(vec![])
        }
    }

    /// Unit tests in Rust are normally defined within such a `#[cfg(test)]`
    /// module and test functions are marked with a `#[test]` attribute.
    /// The below code is technically just normal Rust code.
    #[cfg(test)]
    mod tests {
        /// Imports all the definitions from the outer scope so we can use them here.
        use super::*;
        use sp_core::hexdisplay::HexDisplay;

        /// We test if the default constructor does its job.
        #[ink::test]
        fn account_id_test() {
            let accounts = ink::env::test::default_accounts::<ink::env::DefaultEnvironment>();
            let account_id: &[u8; 32] = accounts.alice.as_ref();

            println!("Account ID: {:?}", HexDisplay::from(account_id));
        }

        /// We test a simple use case of our contract.
        #[ink::test]
        fn encoding_transfer_multiasset() {
            let asset: Box<VersionedMultiAsset> = Box::new(VersionedMultiAsset::V3(MultiAsset {
                id: Concrete(MultiLocation {
                    parents: 1,
                    interior: Here,
                }),
                fun: Fungible(100000),
            }));

            let dest: Box<VersionedMultiLocation> = Box::new((Parent, Parachain(2001)).into());

            let call = types::RuntimeCall::XTokens(types::XTokensCall::TransferMultiAsset {
                asset,
                dest,
                dest_weight_limit: WeightLimit::Unlimited,
            });

            // Expected: 0x37010300010000821a060003010100451f00
            // Actual:   0x37010300010000821a060003010100451f00
            println!("Encoding: {:?}", HexDisplay::from(&call.encode()));
        }
    }

    /// This is how you'd write end-to-end (E2E) or integration tests for ink! contracts.
    ///
    /// When running these you need to make sure that you:
    /// - Compile the tests with the `e2e-tests` feature flag enabled (`--features e2e-tests`)
    /// - Are running a Substrate node which contains `pallet-contracts` in the background
    #[cfg(all(test, feature = "e2e-tests"))]
    mod e2e_tests {
        /// Imports all the definitions from the outer scope so we can use them here.
        use super::*;

        use ink::{
            env::{test::default_accounts, DefaultEnvironment},
            primitives::AccountId,
        };
        /// A helper function used for calling contract messages.
        use ink_e2e::build_message;

        /// The End-to-End test `Result` type.
        type E2EResult<T> = std::result::Result<T, Box<dyn std::error::Error>>;

        /// We test that we can upload and instantiate the contract using its default constructor.
        #[ink_e2e::test]
        async fn default_works(mut client: ink_e2e::Client<C, E>) -> E2EResult<()> {
            // Given
            let constructor = StakePoolRef::new();

            // When
            let contract_account_id = client
                .instantiate("stake_pool", &ink_e2e::alice(), constructor, 100, None)
                .await
                .expect("instantiate failed")
                .account_id;

            // Then
            let get = build_message::<StakePoolRef>(contract_account_id.clone())
                .call(|stake_pool| stake_pool.transfer_to_relay_contract_acc(50));
            let get_result = client.call_dry_run(&ink_e2e::alice(), &get, 0, None).await;
            assert!(matches!(get_result.return_value(), Ok(())));

            Ok(())
        }

        /// We test that we can read and write a value from the on-chain contract contract.
        #[ink_e2e::test]
        async fn basic_tx_works(mut client: ink_e2e::Client<C, E>) -> E2EResult<()> {
            // Given
            let constructor = StakePoolRef::new();
            let contract_account_id = client
                .instantiate("stake_pool", &ink_e2e::bob(), constructor, 100, None)
                .await
                .expect("instantiate failed")
                .account_id;

            let frank = default_accounts::<DefaultEnvironment>().frank;
            let basic_tx = build_message::<StakePoolRef>(contract_account_id.clone())
                .call(|stake_pool| stake_pool.basic_transfer(frank, 10));
            let result = client
                .call_dry_run(&ink_e2e::bob(), &basic_tx, 0, None)
                .await;
            println!("basic tx res: {:?}", result);
            // assert!(matches!(get_result.return_value(), Ok(())));

            Ok(())
        }
    }
}
